package com.korybyrne.domain;

import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;

import static java.time.DayOfWeek.*;
import static org.junit.jupiter.api.Assertions.*;

class SessionTest {
    private static Session makeSession(DayOfWeek day, LocalTime startTime, LocalTime endTime) {
        Session session = new Session();
            session.setDay(day);
            session.setStartTime(startTime);
            session.setEndTime(endTime);
            
        return session;
    }
    
    @Test
    void isOvertimeTest() {
        for (DayOfWeek day : Arrays.asList(MONDAY, TUESDAY, WEDNESDAY, THURSDAY)) {
            assertFalse(makeSession(day, LocalTime.of(9, 0), LocalTime.of(9, 30)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(9, 0), LocalTime.of(9 + 12, 0)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(23, 0), LocalTime.of(23, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(10, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(10+12, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(1, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(1, 0)).isOvertime());
        }

        {
            DayOfWeek day = FRIDAY;
            assertFalse(makeSession(day, LocalTime.of(9, 0), LocalTime.of(9, 30)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(9, 0), LocalTime.of(5 + 12, 0)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(23, 0), LocalTime.of(23, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(10, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(7+12, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(1, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(1, 0)).isOvertime());
        }

        {
            DayOfWeek day = SATURDAY;
            assertFalse(makeSession(day, LocalTime.of(10, 0), LocalTime.of(10, 30)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(10, 0), LocalTime.of(5 + 12, 0)).isOvertime());
            assertFalse(makeSession(day, LocalTime.of(23, 0), LocalTime.of(23, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(10, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(7+12, 0)).isOvertime());

            assertTrue(makeSession(day, LocalTime.of(12, 0), LocalTime.of(1, 0)).isOvertime());
            assertTrue(makeSession(day, LocalTime.of(8, 0), LocalTime.of(1, 0)).isOvertime());
        }

        assertThrows(UnsupportedOperationException.class, () ->
                makeSession(SUNDAY, LocalTime.of(8, 0), LocalTime.of(1, 0)).isOvertime()
        );
    }
}
