package com.korybyrne.domain;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Arrays;

import static java.time.DayOfWeek.MONDAY;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;

class TimeBlockTest {

    @Test
    void partitionTest() {
        assertIterableEquals(
                new TimeBlock(MONDAY, LocalTime.of(9, 30), Duration.ofHours(3)).partition(Duration.ofMinutes(30)),
                Arrays.asList(
                        new TimeBlock(MONDAY, LocalTime.of(9, 30), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(10, 0), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(10, 30), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(11, 0), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(11, 30), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(12, 0), Duration.ofMinutes(30))
                )
        );
        assertIterableEquals(
                new TimeBlock(MONDAY, LocalTime.of(19, 30), Duration.ofMinutes(125)).partition(Duration.ofMinutes(30)),
                Arrays.asList(
                        new TimeBlock(MONDAY, LocalTime.of(19, 30), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(20, 0), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(20, 30), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(21, 0), Duration.ofMinutes(30)),
                        new TimeBlock(MONDAY, LocalTime.of(21, 30), Duration.ofMinutes(5))
                )
        );
    }
}
