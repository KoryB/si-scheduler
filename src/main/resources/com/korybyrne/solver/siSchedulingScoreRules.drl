

package com.korybyrne.solver;
    dialect "java"

import java.util.ArrayList;
import java.time.DayOfWeek;

import com.korybyrne.domain.*;
import com.korybyrne.domain.si.*;
import com.korybyrne.domain.course.*;

import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScoreHolder;

import accumulate com.korybyrne.solver.SessionTypesAccumulator totalBySessionType;
import accumulate com.korybyrne.solver.LongestContiguousBlockAccumulator longestContiguousBlock;

global HardMediumSoftScoreHolder scoreHolder;

/*
    It would be good to have an ability to have a 'break session' in the spreadsheet.
        Some students have extended hours where they're in with the students between science classes for ~8 hours a day.

    Would be nice to have a rule to bias sessions either before or after the class.

    End Goal:
        Take in multiple availability spreadsheets

        XML document
        Coleen document
        Outlook Calender

        Able to tweak and add sessions easily to whatever output is given.
*/

// ############################################################################
// Queries
// ############################################################################

query sessionsOfType(String departmentCode)
    Session(isOfCourseCode(departmentCode))
end

// ############################################################################
// Hard constraints
// ############################################################################

rule "Requested Hours Overage"
    when
        $si : SupplementalInstructor($requestedHours : maximumRequestedHours)
        accumulate(
            $session : Session(supplementalInstructor == $si, $sessionHours : hours);
            $totalHours : sum($sessionHours);
            $totalHours > $requestedHours
        )
    then
        scoreHolder.addHardConstraintMatch(kcontext, (int) (($requestedHours - $totalHours)*2));
end

rule "Busy Overlap"
    when
        $session : Session($si : supplementalInstructor, $overlap : calculateBusyOverlap(), $overlap > 0)
    then
        scoreHolder.addHardConstraintMatch(kcontext, -$overlap);
end

rule "Course Overlap -- single section"
    when
        $session : Session($courses : courses, classSession == false)
        $course  : Course(sectionCount == 1) from $courses
    then
        scoreHolder.addHardConstraintMatch(kcontext, -$session.calculateSectionOverlap($course));
end

rule "Session Overlap"
    when
        $session  : Session($si : supplementalInstructor, $id : id)
        $session2 : Session(supplementalInstructor == $si, id > $id, calculateOverlap($session).getSeconds() > 0)
    then
        scoreHolder.addHardConstraintMatch(kcontext, -100);
end

rule "Session Time Past"
    when
        $session : Session(classSession == false, isOvertime()==true)
    then
        scoreHolder.addHardConstraintMatch(kcontext, -100);
end

// ############################################################################
// Medium constraints
// ############################################################################

// TODO:kb: Fix this to be general with the previous rule
rule "Course Overlap -- multiple sections"
    when
        $session : Session($courses : courses, classSession == false)
        $course  : Course(sectionCount > 1) from $courses
    then
        scoreHolder.addHardConstraintMatch(kcontext, -$session.calculateSectionOverlap($course));
end

rule "Requested Hours Underage"
    when
        $si : SupplementalInstructor($requestedHours : minimumRequestedHours)
        $totalHours : Number($requestedHours > doubleValue)
                      from accumulate($session : Session(supplementalInstructor == $si, $sessionHours : hours),
                                      sum($sessionHours) );
    then
        scoreHolder.addMediumConstraintMatch(kcontext, (int) (($totalHours.doubleValue() - $requestedHours)*2));
end

rule "Session too long -- simple"
    enabled false
    when
        Session($hours : hours, $hours > 4)
    then
        scoreHolder.addMediumConstraintMatch(kcontext, (int)(8 - 2*$hours));
end

rule "Session too long"
    enabled true
    when
        $si : SupplementalInstructor()
        $day : DayOfWeek() from DayOfWeek.values()
        accumulate($session : Session(supplementalInstructor == $si, day == $day);
            $longestBlock : longestContiguousBlock($session);
            $longestBlock > 4.0)
    then
        scoreHolder.addMediumConstraintMatch(kcontext, (int) (-2 * $longestBlock));
end


// ############################################################################
// Soft constraints
// ############################################################################

rule "Don't have too many sessions"
    enabled false
    when
        accumulate($session : Session(hours != 0),
            $count : count($session))
    then
        scoreHolder.addSoftConstraintMatch(kcontext, (int) (-1500 * $count));
end

// TODO:kb: ask Duane why having (2 - $standardDeviation) would cause that weird hangup
rule "Distribute Hours Evenly"
    when
        $si : SupplementalInstructor()
        accumulate($session : Session(supplementalInstructor == $si),
            $totals : totalBySessionType($session)
        )
        accumulate($total : Number() from $totals,
            $variance : variance($total)
        )
    then
        scoreHolder.addSoftConstraintMatch(kcontext, (int) (-1000 * $variance));
end

rule "Maximize Requested Hours"
    when
        $si : SupplementalInstructor($requestedHours : maximumRequestedHours)
        $totalHours : Number($requestedHours > doubleValue)
                      from accumulate($session : Session(supplementalInstructor == $si, $sessionHours : hours),
                                      sum($sessionHours) );
    then
        scoreHolder.addSoftConstraintMatch(kcontext, (int) (($totalHours.doubleValue() - $requestedHours)*2000));
end

