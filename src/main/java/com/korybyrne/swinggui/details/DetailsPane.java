package com.korybyrne.swinggui.details;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.Section;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleComponent;

import javax.swing.*;
import java.awt.*;

public class DetailsPane extends JPanel implements ScheduleComponent {
    public enum Type {
        None, Busy, Class, Session
    }

    private ScheduleMakerController controller;
    private CardLayout layout;

    private JPanel noneDetails;
    private BusyDetailsPanel busyDetails;
    private ClassDetailsPanel classDetails;
    private SessionDetailsPanel sessionDetails;

    public DetailsPane() {
        layout = new CardLayout();
        noneDetails = makeNonePanel();
        busyDetails = new BusyDetailsPanel();
        classDetails = new ClassDetailsPanel();
        sessionDetails = new SessionDetailsPanel();

        this.setLayout(this.layout);

        this.add(noneDetails, Type.None.name());
        this.add(busyDetails, Type.Busy.name());
        this.add(classDetails, Type.Class.name());
        this.add(sessionDetails, Type.Session.name());

        showNoneDetails();
    }

    private void showPanel(Type type)
    {
        layout.show(this, type.name());
    }

    public void showNoneDetails()
    {
        showPanel(Type.None);
    }

    public void showBusyDetails(TimeBlock timeBlock)
    {
        busyDetails.prepareToShow(timeBlock);
        showPanel(Type.Busy);
    }

    public void showClassPanel(Course course, Section section, ClassMeeting meeting)
    {
        classDetails.prepareToShow(course, section, meeting);
        showPanel(Type.Class);
    }

    public void showSessionPanel(Session session) {
        sessionDetails.prepareToShow(session);
        showPanel(Type.Session);
    }

    private static JPanel makeNonePanel() {
        JPanel panel = new JPanel();
        panel.setBackground(Color.darkGray.darker());

        return panel;
    }


    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
        busyDetails.setController(controller);
        classDetails.setController(controller);
        sessionDetails.setController(controller);
    }
}
