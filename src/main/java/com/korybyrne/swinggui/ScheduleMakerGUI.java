package com.korybyrne.swinggui;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.persistence.SIScheduleGenerator;
import com.korybyrne.swinggui.configuration.ConfigurationPanel;
import com.korybyrne.swinggui.details.DetailsPane;
import com.korybyrne.swinggui.instructors.InstructorsPane;
import com.korybyrne.swinggui.schedule.SchedulePanel;
import com.korybyrne.swinggui.selector.SelectorPane;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.ExecutionException;

public class ScheduleMakerGUI implements ActionListener {
    public static NumberFormat decimalFormatter = new DecimalFormat("##.#");

    private Logger logger = LoggerFactory.getLogger(getClass());

    private JFrame window;

    private SchedulePanel schedulePanel;
    private ConfigurationPanel configurationPanel;
    private SelectorPane selectorPane;
    private DetailsPane detailsPane;
    private InstructorsPane instructorsPane;

    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem importSIMenuItem;

    private ScheduleMakerController controller;

    public ScheduleMakerGUI() {

        schedulePanel = new SchedulePanel();
        selectorPane = new SelectorPane();
        detailsPane = new DetailsPane();
        instructorsPane = new InstructorsPane();
        configurationPanel = new ConfigurationPanel();
;
        controller = new ScheduleMakerController(schedulePanel, detailsPane, instructorsPane);
        controller.observeScheduleComponents(schedulePanel, configurationPanel, selectorPane, detailsPane, instructorsPane);
        controller.setSchedule(new SISchedule());

        window = new JFrame("SI Schedule Maker");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLayout(new BorderLayout());
        window.setResizable(true);

        this.addMenuComponents();
        this.addUIComponents();

        window.setPreferredSize(new Dimension(1280, 720));
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);

        detailsPane.setPreferredSize(detailsPane.getMinimumSize());
    }

    private void addMenuComponents() {
        menuBar = new JMenuBar();
            fileMenu = new JMenu("File");
                importSIMenuItem = new JMenuItem("Import SI Spreadsheets");
                    importSIMenuItem.addActionListener(this);
                fileMenu.add(importSIMenuItem);
            menuBar.add(fileMenu);

        window.setJMenuBar(menuBar);
    }

    private void addUIComponents() {
        Container content = window.getContentPane();

//        JSplitPane selectorScheduleSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, selectorPane, schedulePanel);
//        JSplitPane allMiddleSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, selectorScheduleSplit, detailsPane);
//
//        JSplitPane configMiddleSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configurationPanel, allMiddleSplit);
//        JSplitPane allSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, configMiddleSplit, instructorsPane);
//
//        content.add(BorderLayout.CENTER, allSplit);

        content.add(BorderLayout.PAGE_START, configurationPanel);
        content.add(BorderLayout.LINE_START, selectorPane);
        content.add(BorderLayout.CENTER, schedulePanel);
        content.add(BorderLayout.LINE_END, detailsPane);
        content.add(BorderLayout.PAGE_END, instructorsPane);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object source = actionEvent.getSource();

        if (source == importSIMenuItem) {
            new ScheduleGenerationTask().execute();
        }
    }

    private class ScheduleGenerationTask extends SwingWorker<SISchedule, Void> {
        @Override
        public SISchedule doInBackground() {
            SISchedule unfinishedSchedule = new SIScheduleGenerator().generateSchedule(
                    "/com/korybyrne/persistence/courses.xml", 2);

            SolverFactory<SISchedule> siScheduleSolverFactory = SolverFactory.createFromXmlResource(
                    "com/korybyrne/solver/siSchedulingSolverConfig.xml"
            );

            Solver<SISchedule> siScheduleSolver = siScheduleSolverFactory.buildSolver();

            siScheduleSolver.solve(unfinishedSchedule);
            SISchedule bestSchedule = siScheduleSolver.getBestSolution();
            bestSchedule.removeEmptySessions();

            return bestSchedule;
        }

        @Override
        protected void done() {
            try {
                logger.info("Almost done...");
                controller.setSchedule(get());
                logger.info("Done, repainting.");
                configurationPanel.repaint();
                selectorPane.repaint();
            } catch (InterruptedException | ExecutionException ignore) {

            }
        }
    }
}
