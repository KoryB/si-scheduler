package com.korybyrne.swinggui.selector;

import com.korybyrne.domain.SISchedule;
import com.korybyrne.domain.Session;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleComponent;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SelectorPane extends JTabbedPane implements ListSelectionListener, ScheduleComponent {
    private ScheduleMakerController controller;

    private JList<SupplementalInstructor> siJList;
    private JList<Course> courseJList;

    public SelectorPane() {

    }

    private void populateTabs() {
        SISchedule schedule = controller.getSchedule();

        ArrayList<SupplementalInstructor> temp = new ArrayList<>();

        for (SupplementalInstructor si : schedule.getSupplementalInstructors()) {
            IntStream.range(0, 20).forEach(i -> temp.add(si));
        }

        this.siJList = new JList<>(temp.toArray(new SupplementalInstructor[0]));
        JScrollPane siScrollPane = new JScrollPane(siJList);

        this.courseJList = new JList<>(schedule.getCourseList().toArray(new Course[0]));
        JScrollPane courseScrollPane = new JScrollPane(courseJList);

        JLabel departmentSelectionLabel = new JLabel("Department");

        this.addTab("SI", siScrollPane);
        this.addTab("Course", courseScrollPane);
        this.addTab("Department", departmentSelectionLabel);

        this.setTabPlacement(JTabbedPane.NORTH);

        siJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        siJList.addListSelectionListener(this);

        courseJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        courseJList.addListSelectionListener(this);
    }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        Object oSource = listSelectionEvent.getSource();

        if (oSource == siJList) {
            JList<SupplementalInstructor> source = (JList<SupplementalInstructor>) oSource;
            controller.setActiveSIs(source.getSelectedValuesList());
        } else if (oSource == courseJList){
            JList<Course> source = (JList<Course>) oSource;
            controller.setActiveSIs(
                    controller.getSchedule().getSessions().stream()
                            .filter(session -> !Collections.disjoint(session.getCourses(), source.getSelectedValuesList()))
                            .map(Session::getSupplementalInstructor)
                            .collect(Collectors.toSet())
            );
        }
    }

    @Override
    public void onScheduleChanged() {
        this.removeAll();
        this.populateTabs();

        this.revalidate();
        this.repaint();
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
