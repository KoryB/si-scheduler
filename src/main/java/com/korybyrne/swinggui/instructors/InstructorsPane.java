package com.korybyrne.swinggui.instructors;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleMakerController;
import com.korybyrne.swinggui.ScheduleComponent;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

import static com.korybyrne.swinggui.ScheduleMakerGUI.decimalFormatter;

public class InstructorsPane extends JScrollPane implements ScheduleComponent {
    private ScheduleMakerController controller;
    private JPanel instructorContainer;


    public InstructorsPane() {
        super(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.instructorContainer = new JPanel();

        this.setViewportView(instructorContainer);

        instructorContainer.setBackground(Color.ORANGE);
    }

    @Override
    public void onScheduleChanged() {
        instructorContainer.removeAll();

        controller.getActiveSIs().forEach(si -> instructorContainer.add(makeSIPanel(si)));

        this.revalidate();
        this.repaint();
    }

    private JPanel makeSIPanel(SupplementalInstructor si) {
        JPanel siPanel = new JPanel();
        Collection<Session> siSessions = controller.getSchedule().getSessionsForSI(si);
        double hours = siSessions.stream().mapToDouble(Session::getHours).sum();

        siPanel.setLayout(new BoxLayout(siPanel, BoxLayout.PAGE_AXIS));
        siPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        siPanel.add(new JLabel(si.getName() + " : " + si.getSsuId()));
        siPanel.add(new JLabel("Total SI Hours: " + decimalFormatter.format(hours)));
        siPanel.add(new JLabel("Total Sessions: " + siSessions.size()));
        siPanel.add(new JLabel("Minimum Requested Hours: " + decimalFormatter.format(si.getMinimumRequestedHours())));
        siPanel.add(new JLabel("Minimum Requested Hours: " + decimalFormatter.format(si.getMaximumRequestedHours())));

        return siPanel;
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
