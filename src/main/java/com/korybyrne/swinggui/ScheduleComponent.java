package com.korybyrne.swinggui;

public interface ScheduleComponent {
    default void onScheduleChanged() {

    }

    void setController(ScheduleMakerController controller);
}
