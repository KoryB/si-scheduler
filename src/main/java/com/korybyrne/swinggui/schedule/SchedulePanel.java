package com.korybyrne.swinggui.schedule;

import com.korybyrne.domain.Session;
import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.course.Section;
import com.korybyrne.domain.si.SupplementalInstructor;
import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public class SchedulePanel extends JLayeredPane implements ScheduleComponent {
    private static final Color sessionColor = new Color(27, 231, 58);
    private static final Color classMeetingColor = new Color(240, 43, 25);
    private static final Color busyColor = new Color(190, 220, 213);

    private static final Logger logger = LoggerFactory.getLogger(SchedulePanel.class);

    private ScheduleMakerController controller;
    private LayoutManager scheduleLayout;

    public SchedulePanel() {
        this.scheduleLayout = new ScheduleLayout(
                DayOfWeek.MONDAY, DayOfWeek.SATURDAY,
                LocalTime.of(9, 00), LocalTime.of(12 + 9, 00),
                this
        );

        this.setLayout(this.scheduleLayout);
    }

    @Override
    public void onScheduleChanged() {
        this.removeAll();

        // sorted order to prevent overlaps
        SortedSet<ComponentTimeBlock> components = new TreeSet<>();

        for (SupplementalInstructor supplementalInstructor : controller.getActiveSIs()) {
            if (controller.isShowingClassMeetings()) {
                CourseSchedule courseSchedule = supplementalInstructor.getCourseSchedule();
                Collection<Course> courseCollection = courseSchedule.getCourses();
                for (Course course : courseCollection) {
                    for (Section section : course.getSections()) {
                        for (ClassMeeting meeting : section.getClassMeetings()) {
                            JPanel meetingPanel = makeClassMeetingPanel(course, section, meeting);
                            components.add(new ComponentTimeBlock(meetingPanel, meeting, 1));
                        }
                    }
                }
            }

            if (controller.isShowingBusyTimes()) {
                for (TimeBlock busyTime : supplementalInstructor.getBusyTimes()) {
                    JPanel busyPanel = makeBusyPanel(busyTime);
                    components.add(new ComponentTimeBlock(busyPanel, busyTime, 0));
                }
            }
        }

        if (controller.isShowingSessions()) {
            for (Session session : controller.getSchedule().getSessions()) {
                if (controller.isActiveSI(session.getSupplementalInstructor())) {
                    JPanel sessionPanel = makeSessionPanel(session);
                    components.add(new ComponentTimeBlock(sessionPanel, session, 2));
                }
            }
        }

        int i = 0;
        for (ComponentTimeBlock ctb : components) {
            this.add(ctb.component, ctb.timeBlock);
            this.setComponentZOrder(ctb.component, i++);
        }

        this.revalidate();
        this.repaint();
    }

    //////////////////
    // Drawing
    //////////////////

    @Override
    protected void paintComponent(Graphics g) {
        int w = getWidth();
        int h = getHeight();
        int hours = 12;
        int days = 6;

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.cyan);
        g2d.fillRect(0, 0, w, h);

        g2d.setColor(Color.black);
        for (float hour = 0; hour <= hours; hour++) {
            int y = (int) ((hour / hours) * h);

            g2d.drawLine(0, y, w, y);
        }

        for (float day = 0; day <= days; day++) {
            int x = (int) ((day / days) * w);

            g2d.drawLine(x, 0, x, h);
        }
    }

    private static class ComponentTimeBlock implements Comparable<ComponentTimeBlock> {
        private static long ID = 0;

        private Component component;
        private TimeBlock timeBlock;
        private int precedence;
        private long id = ID++;

        public ComponentTimeBlock(Component component, TimeBlock timeBlock, int precedence) {
            this.component = component;
            this.timeBlock = timeBlock;
            this.precedence = precedence;
        }

        @Override
        public int compareTo(@NotNull ComponentTimeBlock rhs) {
            return - new CompareToBuilder()
                    .append(timeBlock.getStartTime(), rhs.timeBlock.getStartTime())
                    .append(precedence, rhs.precedence)
                    .append(id, rhs.id)
                    .toComparison();
        }
    }

    private abstract class BlockMouseListener implements MouseListener {
        private final Color baseColor;
        private final Component component;

        BlockMouseListener(Component component, Color baseColor) {
            this.component = component;
            this.baseColor = baseColor;
        }

        @Override
        public abstract void mouseClicked(MouseEvent mouseEvent);

        @Override
        public void mousePressed(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {

        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            changeComponentColor(baseColor.darker());
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            changeComponentColor(baseColor);
        }

        protected void changeComponentColor(Color newColor) {
            this.component.setBackground(newColor);
            repaint();
        }
    }


    private JPanel makeSessionPanel(Session session) {
        JPanel sessionPanel = new JPanel(new BorderLayout());
        JPanel coursePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        for (Course course : session.getCourses()) {
            JLabel courseLabel = new JLabel(course.getCourseCode());

            coursePanel.add(courseLabel);
        }

        JLabel siLabel = new JLabel(session.getSupplementalInstructor().getName());

        sessionPanel.add(BorderLayout.PAGE_START, siLabel);
        sessionPanel.add(BorderLayout.CENTER, coursePanel);

        setPanelLooks(sessionPanel, sessionColor);
        coursePanel.setBackground(sessionColor);

        sessionPanel.addMouseListener(new BlockMouseListener(sessionPanel, sessionColor) {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                controller.onSessionClicked(session);
            }
        });

        return sessionPanel;
    }

    private JPanel makeClassMeetingPanel(Course course, Section section, ClassMeeting classMeeting) {
        JPanel classMeetingPanel = new JPanel(new BorderLayout());
        JLabel classMeetingLabel = new JLabel(classMeeting.toString());

        classMeetingPanel.add(BorderLayout.PAGE_START, classMeetingLabel);
        setPanelLooks(classMeetingPanel, classMeetingColor);

        classMeetingPanel.addMouseListener(new BlockMouseListener(classMeetingPanel, classMeetingColor) {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                controller.onClassClicked(course, section, classMeeting);
            }
        });

        return classMeetingPanel;
    }

    private JPanel makeBusyPanel(TimeBlock timeBlock) {
        JPanel busyPanel = new JPanel();
        JLabel busyLabel = new JLabel("BUSY");

        busyPanel.add(busyLabel);

        setPanelLooks(busyPanel, busyColor);

        busyPanel.addMouseListener(new BlockMouseListener(busyPanel, busyColor) {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                controller.onBusyClicked(timeBlock);
            }
        });

        return busyPanel;
    }

    private static void setPanelLooks(JPanel panel, Color bgColor) {
        panel.setBackground(bgColor);
        panel.setBorder(
                BorderFactory.createLineBorder(Color.BLACK, 1, true)
        );
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
