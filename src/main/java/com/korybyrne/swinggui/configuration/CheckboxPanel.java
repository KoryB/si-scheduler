package com.korybyrne.swinggui.configuration;

import com.korybyrne.swinggui.ScheduleComponent;
import com.korybyrne.swinggui.ScheduleMakerController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CheckboxPanel extends JPanel implements ItemListener, ScheduleComponent {
    private ScheduleMakerController controller;

    private final JCheckBox busyBox, classBox, sessionBox;

    public CheckboxPanel() {
        busyBox    = new JCheckBox("Show Busy", true);
        classBox   = new JCheckBox("Show Class Meetings", true);
        sessionBox = new JCheckBox("Show Sessions", true);

        busyBox.addItemListener(this);
        classBox.addItemListener(this);
        sessionBox.addItemListener(this);

        this.setLayout(new FlowLayout());
        this.add(busyBox);
        this.add(classBox);
        this.add(sessionBox);
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {
        controller.setShowingBusyTimes(busyBox.isSelected());
        controller.setShowingClassMeetings(classBox.isSelected());
        controller.setShowingSessions(sessionBox.isSelected());
    }

    @Override
    public void setController(ScheduleMakerController controller) {
        this.controller = controller;
    }
}
