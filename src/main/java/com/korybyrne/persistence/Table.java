package com.korybyrne.persistence;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Table<T> {
    private List<List<Cell<T>>> data;
    private int height;
    private int width;

    public Table(T[][] data) {
        this(
                Arrays.stream(data).map(Arrays::asList).collect(Collectors.toList())
        );
    }

    public Table(List<List<T>> data) {
        if (!data.stream().allMatch(list -> list.size() == data.get(0).size()))
            throw new IllegalArgumentException("Not all rows of data match in size: \n" + StringUtils.join(data) + "\n" +
                    data.stream().map(List::size).map(Objects::toString).collect(Collectors.joining(", ")));

        this.height = data.size();

        if (this.height == 0) {
            this.width = 0;
        } else {
            this.width = data.get(0).size();
        }

        this.data = new ArrayList<>(height*width);
        int r, c; r = 0;
        for (List<T> row : data) {
            ArrayList<Cell<T>> dataRow = new ArrayList<>(width);

            c = 0;
            for (T datum : row) {
                dataRow.add(new Cell<>(datum, new Table.Position(r, c)));

                c++;
            }

            this.data.add(dataRow);
            r++;
        }
    }

    public Cell<T> get(Position position) {
        return get(position.r, position.c);
    }

    public Cell<T> get(int row, int column) {
        return data.get(row).get(column);
    }

    public Iterator<Cell<T>> rowFirstIterator() {
        return IteratorUtils.chainedIterator(data.stream()
                .map(List::iterator)
                .collect(Collectors.toList()));
    }

    public Iterator<Cell<T>> rowFirstIterator(Position start, Position endInclusive) {
        return IteratorUtils.chainedIterator(data.subList(start.r, endInclusive.r+1).stream()
                .map(list -> list.subList(start.c, endInclusive.c+1))
                .map(List::iterator)
                .collect(Collectors.toList()));
    }

    public Iterator<Cell<T>> columnFirstIterator() {
        return IteratorUtils.chainedIterator(IntStream.range(0, width)
                .mapToObj(ColumnIterator::new)
                .collect(Collectors.toList())
        );
    }

    public Iterator<Cell<T>> columnFirstIterator(Position start, Position endInclusive) {
        return IteratorUtils.chainedIterator(IntStream.range(start.c, endInclusive.c+1)
                .mapToObj(column -> new ColumnIterator(column, start.r, endInclusive.r+1))
                .collect(Collectors.toList())
        );
    }

    // Overrides

    @Override
    public String toString() {
        return StringUtils.join(data);
    }


    // Classes

    public static class Position {
        public int r;
        public int c;

        public Position(int row, int column) {
            r = row;
            c = column;
        }

        @Override
        public String toString() {
            return "(" + r + ", " + c + ")";
        }
    }

    public static class Cell<T> {
        public T data;
        public Position position;

        public Cell(T data, Position position) {
            this.data = data;
            this.position = position;
        }

        @Override
        public String toString() {
            return data + ": " + position;
        }
    }

    private class ColumnIterator implements Iterator<Cell<T>> {
        private final int column;
        private int row;
        private int end;

        ColumnIterator(int column) {
            this(column, 0, height);
        }

        ColumnIterator(int column, int start, int end) {
            this.column = column;
            this.row = start;
            this.end = end;
        }

        @Override
        public boolean hasNext() {
            return row < end;
        }

        @Override
        public Cell<T> next() {
            return data.get(row++).get(column);
        }
    }
}
