package com.korybyrne.domain.si;

import com.korybyrne.domain.TimeBlock;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;

import java.util.*;

public class SupplementalInstructor {
    private String name;
    private int ssuId;

    private RequestedHours requestedHours;

    private Collection<TimeBlock> busyTimes;
    private CourseSchedule courseSchedule;

    public SupplementalInstructor(String name, int ssuId,
                                  RequestedHours requestedHours,
                                  List<TimeBlock> busyTimeList, List<Course> courseList) {
        this.name = name;
        this.ssuId = ssuId;
        this.requestedHours = requestedHours;

        this.busyTimes = new ArrayList<>();
        this.courseSchedule = new CourseSchedule();

        this.busyTimes.addAll(busyTimeList);
        this.courseSchedule.addCourses(courseList);
    }

    public int getSsuId() {
        return ssuId;
    }

    public String getName() {
        return name;
    }

    public Collection<TimeBlock> getBusyTimes() {
        return busyTimes;
    }

    public CourseSchedule getCourseSchedule() {
        return courseSchedule;
    }

    @Override
    public String toString() {
        return name;
    }

    //////////////////
    // complex methods
    //////////////////

    //TODO:kb: generalize, load in from spreadsheet
    public Set<String> getSessionTypes() {
        return new TreeSet<>(Arrays.asList("MATH2110", "MATH2130", "ENGL1101"));
    }

    public void addBusyTime(TimeBlock busyTime) {
        busyTimes.add(busyTime);
    }

    public double getMaximumRequestedHours() {
        return requestedHours.getMaximum().getSeconds() / 3600.0;
    }

    public double getMinimumRequestedHours() {
        return requestedHours.getMinimum().getSeconds() / 3600.0;
    }
}
