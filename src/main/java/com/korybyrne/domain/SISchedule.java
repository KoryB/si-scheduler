package com.korybyrne.domain;

import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.CourseSchedule;
import com.korybyrne.domain.si.SupplementalInstructor;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.score.buildin.hardmediumsoft.HardMediumSoftScore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@PlanningSolution
public class SISchedule {
    private Collection<SupplementalInstructor> supplementalInstructors;
    private CourseSchedule courseSchedule;
    private Collection<Session> sessions;

    private HardMediumSoftScore hardMediumSoftScore;

    public SISchedule() {
        supplementalInstructors = new ArrayList<>();
        courseSchedule = new CourseSchedule();
        sessions = new ArrayList<>();
    }

    public void setSupplementalInstructors(List<SupplementalInstructor> supplementalInstructors) {
        this.supplementalInstructors = supplementalInstructors;
    }

    public void setCourseSchedule(CourseSchedule courseSchedule) {
        this.courseSchedule = courseSchedule;
    }

    public void removeEmptySessions() {
        sessions = sessions.stream()
                .filter(session -> session.getDuration().getSeconds() != 0)
                .collect(Collectors.toList());
    }

    public double getSIHoursForCourse(Course course) {
        return getSessions().stream()
                .filter(session -> session.getCourses().contains(course))
                .mapToDouble(Session::getHours)
                .sum();
    }

    public Collection<Session> getSessionsForSI(SupplementalInstructor si) {
        return getSessions().stream()
                .filter(session -> session.getSupplementalInstructor() == si)
                .collect(Collectors.toList());
    }


    ////////////////////////
    // Optaplanner interop.
    ////////////////////////

    @ProblemFactCollectionProperty
    public Collection<SupplementalInstructor> getSupplementalInstructors() {
        return supplementalInstructors;
    }

    @PlanningEntityCollectionProperty
    public Collection<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    @PlanningScore
    public HardMediumSoftScore getHardMediumSoftScore() {
        return hardMediumSoftScore;
    }

    public void setHardMediumSoftScore(HardMediumSoftScore hardMediumSoftScore) {
        this.hardMediumSoftScore = hardMediumSoftScore;
    }

    @ProblemFactCollectionProperty
    public Collection<Course> getCourseList() {
        return courseSchedule.getCourses();
    }

    @ProblemFactCollectionProperty
    public Collection<ClassMeeting> getClassMeetingList() {
        return courseSchedule.getClassMeetingList();
    }
}
