package com.korybyrne.domain.course;

import com.korybyrne.domain.TimeBlock;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Collection;

/**
 * A simple extension of {@link TimeBlock} used to represent individual class meetings of a {@link Section}.
 */
public class ClassMeeting extends TimeBlock {
    private Section parent;

    /**
     * Constructs an empty class meeting object.
     */
    public ClassMeeting() {
        super();
    }

    /**
     * Constructs a class meeting with given parameters and no parent.
     * <p>
     * The parent will be set when either {@link Section#addClassMeeting(ClassMeeting)} or {@link Section#addClassMeetings(Collection)} is called.
     */
    public ClassMeeting(DayOfWeek day, LocalTime startTime, Duration duration) {
        super(day, startTime, duration);
    }

    /**
     * Gets the parent for this class meeting.
     * @return the parent for this class meeting, or null if there is no parent.
     */
    Section getParent() {
        return parent;
    }

    /**
     * @param section the section to set as the parent. Called by {@link Section#addClassMeeting(ClassMeeting)}.
     */
    void setParent(Section section) {
        this.parent = section;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return parent + ": " + super.toString();
    }
}
