package com.korybyrne.domain;

import com.korybyrne.domain.course.ClassMeeting;
import com.korybyrne.domain.course.Course;
import com.korybyrne.domain.course.Section;
import com.korybyrne.domain.si.SupplementalInstructor;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.entity.PlanningPin;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@PlanningEntity
public class Session extends TimeBlock {
    private static int TIME_GRAIN = 30;

    private int id;
    private SupplementalInstructor supplementalInstructor;
    private Set<Course> courses;

    private boolean classSession;

    public Session() {
        this(DayOfWeek.MONDAY, null, new HashSet<>());
    }

    public Session(DayOfWeek day, SupplementalInstructor si, Course course) {
        this(day, si, Collections.singletonList(course));
    }

    public Session(DayOfWeek day, SupplementalInstructor si, Collection<Course> courseCollection) {
        super(day, LocalTime.of(9, 0), Duration.ofHours(0));

        this.supplementalInstructor = si;
        this.courses = new HashSet<>();
        this.classSession = false;

        courses.addAll(courseCollection);
    }

    public SupplementalInstructor getSupplementalInstructor() {
        return supplementalInstructor;
    }

    public void setSupplementalInstructor(SupplementalInstructor supplementalInstructor) {
        this.supplementalInstructor = supplementalInstructor;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //////////////////
    // complex methods
    //////////////////

    public Set<String> getSessionType() {
        return courses.stream().map(Course::getCourseCode).collect(Collectors.toSet());
    }

    public boolean isOfCourseCode(String departmentCode) {
        return courses.stream().anyMatch(course -> course.getCourseCode().equals(departmentCode));
    }

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    public void setClassSessionMeeting(ClassMeeting meeting) {
        if (meeting == null) {
            this.classSession = false;
        } else {
            this.classSession = true;
            this.setDay(meeting.getDay());
            this.setStartTime(meeting.getStartTime());
            this.setDuration(meeting.getDuration());
        }
    }

    public int getSectionCount() {
        return getCourses().stream().map(Course::getSections).mapToInt(Collection::size).sum();
    }

    public int getCourseCount() {
        return courses.size();
    }

    public boolean isOvertime() {
        return !(getDuration().isZero() || calculateOverlap(getWorkingHours()).equals(getDuration()));
    }


    ////////////////////////
    // Optaplanner interop.
    ////////////////////////

    public TimeBlock getWorkingHours() {
        switch (getDay()) {
            default: // required by compiler
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
                return new TimeBlock(getDay(), LocalTime.of(9, 00), LocalTime.of(9+12, 00));
            case FRIDAY:
                if (getCourses().stream().anyMatch(course -> course.getDepartment().equals("ARTG"))) {
                    return new TimeBlock(getDay(), LocalTime.of(9, 00), LocalTime.of(6 + 12, 00));
                } else {
                    return new TimeBlock(getDay(), LocalTime.of(9, 00), LocalTime.of(5 + 12, 00));
                }
            case SATURDAY:
                if (getCourses().stream().anyMatch(course -> course.getDepartment().equals("ARTG"))) {
                    return new TimeBlock(getDay(), LocalTime.of(10, 00), LocalTime.of(6 + 12, 00));
                } else {
                    return new TimeBlock(getDay(), LocalTime.of(10, 00), LocalTime.of(5 + 12, 00));
                }
            case SUNDAY:
                throw new UnsupportedOperationException("Sunday is not a supported day, session: " + toString());
        }
    }

    @ValueRangeProvider(id = "sessionTimeRange")
    @ProblemFactCollectionProperty
    public CountableValueRange<LocalTime> getTimeRange() {
        TimeBlock range = getWorkingHours();

        return ValueRangeFactory.createLocalTimeValueRange(
                range.getStartTime(), range.getEndTime(), TIME_GRAIN, ChronoUnit.MINUTES
        );
    }

    @ValueRangeProvider(id = "sessionDurationRange")
    @ProblemFactCollectionProperty
    public List<Duration> getDurationRange() {
        List<Duration> rv = IntStream.range(2, (int)(supplementalInstructor.getMaximumRequestedHours()*2))
                .mapToObj(grains -> Duration.ofMinutes(TIME_GRAIN*grains))
                .collect(Collectors.toList());

        rv.add(Duration.ofMinutes(0));

        return rv;
    }

    @PlanningPin
    public boolean isClassSession() {
        return this.classSession;
    }

    @Override
    @PlanningVariable(valueRangeProviderRefs = {"sessionTimeRange"})
    public LocalTime getStartTime() {
        return super.getStartTime();
    }

    @Override
    public void setStartTime(LocalTime startTime) {
        super.setStartTime(startTime);
    }

    @Override
    @PlanningVariable(valueRangeProviderRefs = {"sessionDurationRange"})
    public Duration getDuration() {
        return super.getDuration();
    }

    @Override
    public void setDuration(Duration duration) {
        super.setDuration(duration);
    }


    /////////////////////////////
    // Optaplanner Calculations
    /////////////////////////////


    public Duration calculateClassOverlap(ClassMeeting meeting) {
        return calculateOverlap(meeting);
    }

    public int calculateBusyOverlap() {
        return (int) calculateOverlap(supplementalInstructor.getBusyTimes()).getSeconds() / 60;
    }

    public int calculateCourseOverlap() {
        return (int) calculateOverlap(
                courses.stream()
                        .map(Course::getSections).flatMap(Collection::stream)
                        .map(Section::getClassMeetings).flatMap(Collection::stream)
                        .collect(Collectors.toList())
        ).getSeconds() / 60;
    }

    // Given a course, how many if its sections does this session overlap with
    public int calculateSectionOverlap(Course course) {
        return (int) course.getSections().stream()
                .map(Section::getClassMeetings).flatMap(Collection::stream)
                .filter(classMeeting -> !this.calculateOverlap(classMeeting).isZero())
                .count();
    }


    // Overrides

    @Override
    public String toString() {
        String courseInfo = courses.stream().map(Course::getCourseCode).collect(Collectors.joining(", "));
        return super.toString() + "{" + courseInfo + "}[" + supplementalInstructor + "]";
    }
}

